package elementx.test.playback

import android.os.Bundle
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.widget.ArrayAdapter
import android.widget.Button
import elementx.media.MediaCompatActivity
import elementx.media.elements.setProgressBars
import elementx.media.extensions.isPlaying
import elementx.media.extensions.setQueueList
import elementx.media.mediacatalog.retrieveMediaFromArtist
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : MediaCompatActivity() {
    private lateinit var listAdapter: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }



    override fun onServiceConnected(controller: MediaControllerCompat) {

        controller.setProgressBars(null, null, seekBar, progressBar)

        button.setOnClickListener {
            controller.setProgressBars(progreesTxt, totalTxt, null)
        }


        GlobalScope.launch {
            retrieveMediaFromArtist("1", "2", "3")?.let {
                listAdapter = ArrayAdapter(this@MainActivity, android.R.layout.simple_list_item_1, it.map { it.title?:"" })
                runOnUiThread {
                    listView.adapter = listAdapter
                    listView.setOnItemClickListener { _, _, position, _ ->
                        controller.setQueueList(this@MainActivity, it, position)
                        controller.transportControls.play()
                    }
                }
            }
        }




        btn_go.setOnClickListener {
            if (controller.isPlaying) controller.transportControls.pause()
            else controller.transportControls.play()
        }

        btn_next.setOnClickListener {
            controller.transportControls.skipToNext()
        }

        btn_previous.setOnClickListener {
            controller.transportControls.skipToPrevious()
        }

        btn_shuffle.setOnClickListener {
            controller.transportControls.setShuffleMode(PlaybackStateCompat.SHUFFLE_MODE_ALL)
        }

        btn_repeat.setOnClickListener {
            when {
                controller.repeatMode == PlaybackStateCompat.REPEAT_MODE_ALL -> {
                    controller.transportControls.setRepeatMode(PlaybackStateCompat.REPEAT_MODE_ONE)
                    (it as Button).text = "Repeat One"
                }


                controller.repeatMode == PlaybackStateCompat.REPEAT_MODE_ONE -> {
                    controller.transportControls.setRepeatMode(PlaybackStateCompat.REPEAT_MODE_NONE)
                    (it as Button).text = "Repeat None"
                }

                controller.repeatMode == PlaybackStateCompat.REPEAT_MODE_NONE -> {
                    controller.transportControls.setRepeatMode(PlaybackStateCompat.REPEAT_MODE_ALL)
                    (it as Button).text = "Repeat All"
                }
            }
        }
    }

    override fun onMetadataChanged(metadata: MediaMetadataCompat, queuIndex: Int) {
        img_albumArt.setImageBitmap(metadata.getBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART))
        txt_track_title.text = metadata.getString(MediaMetadataCompat.METADATA_KEY_TITLE)
        txt_track_artist.text = metadata.getString(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE)
        txt_track_album.text = metadata.getString(MediaMetadataCompat.METADATA_KEY_DISPLAY_DESCRIPTION)
    }

    override fun onPlaybackStateChanged(state: PlaybackStateCompat) {}
    override fun onQueueChanged(queue: MutableList<MediaSessionCompat.QueueItem>?) {}
    override fun onRepeatModeChanged(repeatMode: Int) {}
    override fun onShuffleModeChanged(shuffleMode: Int) {}
}
