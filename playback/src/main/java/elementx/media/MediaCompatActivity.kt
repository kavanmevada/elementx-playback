/*
 * Copyright (C) 2019  Kavan Mevada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package elementx.media

import android.content.ComponentName
import android.os.Bundle
import android.os.PersistableBundle
import android.os.RemoteException
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import androidx.appcompat.app.AppCompatActivity
import elementx.media.extensions.getQueuIndex
import elementx.media.extensions.restore
import elementx.media.extensions.restoreQueue
import elementx.media.interfaces.MediaSessionCallback
import elementx.media.models.MediaItem
import elementx.media.services.MusicService


open class MediaCompatActivity : AppCompatActivity(), MediaSessionCallback {
    private lateinit var mediaBrowser: MediaBrowserCompat
    protected var mediaController: MediaControllerCompat? = null

    var mediaSessionListener = arrayListOf<MediaSessionCallback>()


    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        addMediaCallbackListener(this)
    }

    private val controllerCallback = object : MediaControllerCompat.Callback() {
        override fun onPlaybackStateChanged(state: PlaybackStateCompat?) {
            super.onPlaybackStateChanged(state)
            state?.let { mediaSessionListener.forEach { it.onPlaybackStateChanged(state) } }
        }

        override fun onMetadataChanged(metadata: MediaMetadataCompat?) {
            super.onMetadataChanged(metadata)
            metadata?.let {
                mediaSessionListener.forEach {
                    it.onMetadataChanged(
                        metadata,
                        mediaController?.getQueuIndex ?: -1
                    )
                }
            }
        }

        override fun onQueueChanged(queue: MutableList<MediaSessionCompat.QueueItem>?) {
            super.onQueueChanged(queue)
            queue?.let {
                mediaSessionListener.forEach { it.onQueueChanged(queue) }
            }
        }

        override fun onRepeatModeChanged(repeatMode: Int) {
            super.onRepeatModeChanged(repeatMode)
            mediaSessionListener.forEach { it.onRepeatModeChanged(repeatMode) }
        }

        override fun onShuffleModeChanged(shuffleMode: Int) {
            super.onShuffleModeChanged(shuffleMode)
            mediaSessionListener.forEach { it.onShuffleModeChanged(shuffleMode) }
        }
    }


    private val connectionCallback = object : MediaBrowserCompat.ConnectionCallback() {
        override fun onConnected() {
            try {
                mediaController = MediaControllerCompat(this@MediaCompatActivity, mediaBrowser.sessionToken)
                MediaControllerCompat.setMediaController(this@MediaCompatActivity, mediaController)

                mediaController?.let { controller ->
                    controller.registerCallback(controllerCallback)

                    if (!MusicService.serviceInStartedState) {
                        val list = restore<MutableList<MediaItem>>("playingQueue")
                        list?.let {
                            controller.restoreQueue(it)
                            controllerCallback.onQueueChanged(controller.queue)
                        }
                        val position = restore<Int>("queueIndex")?.toLong()
                        position?.let { controller.transportControls.skipToQueueItem(it) }
                    }

                    controllerCallback.onMetadataChanged(controller.metadata)
                    controllerCallback.onPlaybackStateChanged(controller.playbackState)

                    mediaSessionListener.forEach { callbacks -> callbacks.onServiceConnected(controller) }
                }
            } catch (e: RemoteException) {
                throw RuntimeException(e)
            }
        }
    }


    fun addMediaCallbackListener(callback: MediaSessionCallback) = mediaSessionListener.add(callback)
    fun removeMediaCallbackListener(callback: MediaSessionCallback) = mediaSessionListener.remove(callback)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mediaBrowser = MediaBrowserCompat(
            this,
            ComponentName(this, MusicService::class.java),
            connectionCallback, null
        )
        mediaBrowser.connect()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mediaBrowser.isConnected) mediaBrowser.unsubscribe("root")
        mediaBrowser.disconnect()
        mediaSessionListener.clear()

        removeMediaCallbackListener(this)
    }



    override fun onMetadataChanged(metadata: MediaMetadataCompat, queuIndex: Int) {}
    override fun onPlaybackStateChanged(state: PlaybackStateCompat) {}
    override fun onQueueChanged(queue: MutableList<MediaSessionCompat.QueueItem>?) {}
    override fun onRepeatModeChanged(repeatMode: Int) {}
    override fun onShuffleModeChanged(shuffleMode: Int) {}
    override fun onServiceConnected(controller: MediaControllerCompat) {}
}








