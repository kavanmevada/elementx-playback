/*
 * Copyright (C) 2019  Kavan Mevada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package elementx.media.mediacatalog

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore.Audio
import elementx.media.models.*


val mediaItemProjection = arrayOf(
    Audio.Media._ID,
    Audio.Media.TITLE,
    Audio.Media.ARTIST,
    Audio.Media.ALBUM,
    Audio.Media.DURATION,
    Audio.Media.TRACK,
    Audio.Media.DATA,
    Audio.Media.ALBUM_ID,
    Audio.Media.ARTIST_ID
)

val Cursor.mediaItem
    get() = MediaItem(
        getString(0),
        getString(1),
        getString(2),
        getString(3),
        null,
        getLong(4),
        getLong(5),
        getString(6),
        "content://media/external/audio/albumart/${getLong(7)}",
        getString(7),
        getString(8)
    )

val albumItemProjection = arrayOf(
    Audio.Albums._ID,
    Audio.Albums.ALBUM,
    Audio.Albums.ARTIST,
    Audio.Albums.NUMBER_OF_SONGS,
    Audio.Albums.LAST_YEAR,
    Audio.Albums.ALBUM_ART
)

val Cursor.albumItem
    get() = AlbumItem(
        getString(0),
        getString(1),
        getString(2),
        getInt(3),
        getString(4) ?: "",
        "content://media/external/audio/albumart/${getLong(0)}"
    )

val artistItemProjection = arrayOf(
    Audio.Artists._ID,
    Audio.Artists.ARTIST,
    Audio.Artists.NUMBER_OF_ALBUMS,
    Audio.Artists.NUMBER_OF_TRACKS
)

val Cursor.artistItem
    get() = ArtistItem(
        getString(0),
        getString(1),
        getInt(2),
        getInt(3)
    )

val playlistItemProjection = arrayOf(
    Audio.Playlists._ID,
    Audio.Playlists.NAME,
    Audio.Playlists.DATE_ADDED,
    Audio.Playlists.DATE_MODIFIED,
    Audio.Playlists.DATA
)

val Cursor.playlistItem
    get() = PlaylistItem(
        getString(0),
        getString(1),
        getString(2),
        getString(3) ?: "",
        getString(4)
    )

val genreItemProjection = arrayOf(
    Audio.Genres._ID,
    Audio.Genres.NAME
)

val Cursor.genreItem
    get() = GenreItem(
        getString(0),
        getString(1)
    )


fun Context.itemCursor(
    uri: Uri,
    projection: Array<String>,
    selection: Int,
    lookupString: String,
    vararg mediaIds: String = emptyArray()
): Cursor? {
    val string = mediaIds.contentToString().replace("[", "(").replace("]", ")")
    return contentResolver.query(
        uri, projection,
        when {
            lookupString!="" -> projection[selection] + " LIKE ?"
            mediaIds.isNotEmpty() -> projection[selection] + " IN $string"
            else -> null
        }, if (lookupString!="") arrayOf("%$lookupString%") else null, null
    )
}


fun <T> Cursor.retrieveMedia(func: (Cursor) -> T) = mutableListOf<T>().apply {
    this@retrieveMedia.let {
        while (it.moveToNext()) {
            add(func(this@retrieveMedia))
        }
        it.close()
    }
}

fun Context.getMediaGenreCursur(musicId: String): String? {
    var genre: String? = null
    contentResolver.query(
        Audio.Genres.getContentUriForAudioId("external", musicId.toInt()),
        arrayOf(Audio.Genres.NAME, Audio.Genres._ID), null, null, null
    )?.let {
        while (it.moveToNext()) {
            genre = it.getString(0)
        }
        it.close()
    }
    return genre
}


fun Context.retrieveMedia(vararg mediaIds: String = emptyArray()) =
    itemCursor(
        Audio.Media.EXTERNAL_CONTENT_URI,
        mediaItemProjection, 0, "",
        *mediaIds
    )?.retrieveMedia { it.mediaItem }

fun Context.findInMedia(title: String) =
    itemCursor(
        Audio.Media.EXTERNAL_CONTENT_URI,
        mediaItemProjection, 1, title
    )?.retrieveMedia { it.mediaItem }


fun Context.retrieveAlbum(vararg albumIds: String): MutableList<AlbumItem>? {
    return itemCursor(
        Audio.Albums.EXTERNAL_CONTENT_URI,
        albumItemProjection, 0, "",
        *albumIds
    )?.retrieveMedia { it.albumItem }
}

fun Context.findInAlbum(title: String) =
    itemCursor(
        Audio.Albums.EXTERNAL_CONTENT_URI,
        albumItemProjection, 1, title
    )?.retrieveMedia { it.albumItem }


fun Context.retrieveArtist(vararg artistIds: String = emptyArray()) =
    itemCursor(
        Audio.Artists.EXTERNAL_CONTENT_URI,
        artistItemProjection, 0,"",
        *artistIds
    )?.retrieveMedia { it.artistItem }

fun Context.findInArtist(title: String) =
    itemCursor(
        Audio.Artists.EXTERNAL_CONTENT_URI,
        artistItemProjection, 1, title
    )?.retrieveMedia { it.artistItem }


fun Context.retrievePlaylist(vararg playlistIds: String = emptyArray()) =
    itemCursor(
        Audio.Playlists.EXTERNAL_CONTENT_URI,
        playlistItemProjection, 0,"",
        *playlistIds
    )?.retrieveMedia { it.playlistItem }


fun Context.retrieveGenre(vararg genreIds: String = emptyArray()) =
    itemCursor(
        Audio.Genres.EXTERNAL_CONTENT_URI,
        genreItemProjection, 0,"",
        *genreIds
    )?.retrieveMedia { it.genreItem }


fun Context.retrieveMediaFromAlbum(vararg albumIds: String) =
    itemCursor(
        Audio.Media.EXTERNAL_CONTENT_URI,
        mediaItemProjection, 7,"",
        *albumIds
    )?.retrieveMedia { it.mediaItem }

fun Context.retrieveMediaFromArtist(vararg artistIds: String) =
    itemCursor(
        Audio.Media.EXTERNAL_CONTENT_URI,
        mediaItemProjection, 8,"",
        *artistIds
    )?.retrieveMedia { it.mediaItem }

fun Context.retrieveMediaFromPlaylist(playlistId: String) =
    itemCursor(
        Audio.Playlists.Members.getContentUri("external", playlistId.toLong()),
        mediaItemProjection, 0, ""
    )?.retrieveMedia { it.mediaItem }

fun Context.retrieveMediaFromGenre(genreId: String) =
    itemCursor(
        Audio.Genres.Members.getContentUri("external", genreId.toLong()),
        mediaItemProjection, 0,""
    )?.retrieveMedia { it.mediaItem }