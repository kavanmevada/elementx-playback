package elementx.media.mediacatalog

import android.content.Context
import android.net.Uri
import android.provider.MediaStore


fun Context.itemSearchCursor(uri: Uri, projection: Array<String>, selection: Int, paramString: String) =
    contentResolver.query(
        uri, projection,
        "${projection[selection]} LIKE ?",
        arrayOf("%$paramString%"),
        null
    )


fun Context.searchInMedia(queryString: String) = itemSearchCursor(
    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
    mediaItemProjection, 1, queryString
)?.retrieveMedia { it.mediaItem }

fun Context.searchInAlbum(queryString: String) = itemSearchCursor(
    MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
    albumItemProjection, 1, queryString
)?.retrieveMedia { it.albumItem }

fun Context.searchInArtist(queryString: String) = itemSearchCursor(
    MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI,
    artistItemProjection, 1, queryString
)?.retrieveMedia { it.artistItem }

fun Context.searchInPlaylist(queryString: String) = itemSearchCursor(
    MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
    playlistItemProjection, 1, queryString
)?.retrieveMedia { it.playlistItem }

fun Context.searchInGenre(queryString: String) = itemSearchCursor(
    MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI,
    genreItemProjection, 1, queryString
)?.retrieveMedia { it.genreItem }