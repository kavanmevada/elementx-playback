/*
 * Copyright (C) 2019  Kavan Mevada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package elementx.media.models

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.provider.MediaStore
import android.support.v4.media.MediaDescriptionCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaSessionCompat
import android.util.Log
import java.io.Serializable

class MediaItem(
    val mediaId: String?, val title: String?,
    val artist: String?, var album: String?,
    var genre: String?, val duration: Long, val trackNo: Long,
    val mediaUri: String?, val coverUri: String?,
    val albumId: String?, val artistId: String?
) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(mediaId)
        parcel.writeString(title)
        parcel.writeString(artist)
        parcel.writeString(album)
        parcel.writeString(genre)
        parcel.writeLong(duration)
        parcel.writeLong(trackNo)
        parcel.writeString(mediaUri)
        parcel.writeString(coverUri)
        parcel.writeString(albumId)
        parcel.writeString(artistId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MediaItem> {
        override fun createFromParcel(parcel: Parcel): MediaItem {
            return MediaItem(parcel)
        }

        override fun newArray(size: Int): Array<MediaItem?> {
            return arrayOfNulls(size)
        }
    }
}

val MediaItem.description
    get() = MediaDescriptionCompat.Builder()
        .setTitle(title)
        .setSubtitle(artist)
        .setDescription(album)
        .setMediaId(mediaId)
        .setMediaUri(Uri.parse(mediaUri))
        .setIconUri(Uri.parse(coverUri))
        .setExtras(Bundle().apply {
            putLong("duration", duration)
        })
        .build()

fun MediaSessionCompat.QueueItem.metadataCompat(context: Context): MediaMetadataCompat? {


    return MediaMetadataCompat.Builder().apply {
        putText(MediaMetadataCompat.METADATA_KEY_TITLE, description.title)
        putText(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE, description.subtitle)
        putText(MediaMetadataCompat.METADATA_KEY_DISPLAY_DESCRIPTION, description.description)
        putLong(MediaMetadataCompat.METADATA_KEY_DURATION, description.extras?.getLong("duration") ?: 0)

        try {
            val coverBitmap = MediaStore.Images.Media.getBitmap(
                context.contentResolver, description.iconUri
            )

            putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, coverBitmap)
        } catch (e: Exception) {
            Log.w(javaClass.canonicalName, "Cover art not found at uri ${description.iconUri}")
        }

    }.build()
}

