/*
 * Copyright (C) 2019  Kavan Mevada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package elementx.media.extensions

import android.content.Context
import android.os.Bundle
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.PlaybackStateCompat
import elementx.media.models.MediaItem
import elementx.media.models.description


val MediaControllerCompat.isPlaying
    get() =
        playbackState != null && playbackState.state == PlaybackStateCompat.STATE_PLAYING


fun MediaControllerCompat.setQueueList(context: Context, list: List<MediaItem>, position: Int = 0) {
    val bundle = Bundle().apply { putParcelableArray("queue", list.toTypedArray()) }
    transportControls.sendCustomAction("setQueue", bundle)
    context.store("playingQueue", list)
    transportControls.skipToQueueItem(position.toLong())
}

fun MediaControllerCompat.restoreQueue(list: List<MediaItem>) {
    val bundle = Bundle().apply { putParcelableArray("queue", list.toTypedArray()) }
    transportControls.sendCustomAction("setQueue", bundle)
}

val MediaControllerCompat.getQueuIndex get() = extras?.getInt("position")

val MediaControllerCompat.getCurrentMediaId get() = extras?.getString("current_mediaid")

fun MediaControllerCompat.addMediaToPlayingQueue(context: Context, item: MediaItem) {
    addQueueItem(item.description)
    context.store("playingQueue", queue)
}