package elementx.media

import android.app.Activity
import android.content.Context
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import androidx.fragment.app.Fragment
import elementx.media.interfaces.MediaSessionCallback

open class MediaCompatFragment : Fragment(), MediaSessionCallback {

    protected var mediaController: MediaControllerCompat? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mediaController = MediaControllerCompat.getMediaController(activity as Activity)
    }

    override fun onStart() {
        super.onStart()
        (context as MediaCompatActivity).addMediaCallbackListener(this)
    }

    override fun onPause() {
        (context as MediaCompatActivity).removeMediaCallbackListener(this)
        super.onPause()
    }

    override fun onMetadataChanged(metadata: MediaMetadataCompat, queuIndex: Int) {}
    override fun onPlaybackStateChanged(state: PlaybackStateCompat) {}
    override fun onQueueChanged(queue: MutableList<MediaSessionCompat.QueueItem>?) {}
    override fun onRepeatModeChanged(repeatMode: Int) {}
    override fun onShuffleModeChanged(shuffleMode: Int) {}
    override fun onServiceConnected(controller: MediaControllerCompat) {}
}