package elementx.media.elements

import android.animation.ValueAnimator
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.view.animation.LinearInterpolator
import android.widget.ProgressBar
import android.widget.SeekBar
import android.widget.TextView

private var progressAnimator = ValueAnimator()


fun MediaControllerCompat.setProgressBars(
    progressLabel: TextView? = null,
    remainLabel: TextView? = null,
    vararg bars: ProgressBar? = emptyArray()
) {
    progressAnimator.cancel()
    var maxProgress = playbackState?.extras?.getInt("duration") ?: 0
    var progressPos = playbackState?.position?.toInt() ?: 0

    val seekbarListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {}
        override fun onStartTrackingTouch(seekBar: SeekBar) {
            progressAnimator.cancel()
        }

        override fun onStopTrackingTouch(seekBar: SeekBar) {
            progressAnimator.cancel()
            transportControls.seekTo(seekBar.progress.toLong())
            seekBar.updateSeekBar(playbackState, seekBar.progress, maxProgress)
        }
    }

    bars.forEach {
        it?.max = maxProgress
        if (it is SeekBar) it.setOnSeekBarChangeListener(seekbarListener)
        playbackState?.let { state -> it?.updateSeekBar(state, progressPos, maxProgress) }
    }
    playbackState?.let { state -> progressLabel?.updateSeekBar(state, progressPos, maxProgress) }
    playbackState?.let { state -> remainLabel?.updateSeekBar(state, progressPos, maxProgress, true) }


    val callack = object : MediaControllerCompat.Callback() {
        override fun onPlaybackStateChanged(state: PlaybackStateCompat?) {
            state?.let {
                progressAnimator.cancel()

                maxProgress = playbackState?.extras?.getInt("duration") ?: 0
                progressPos = state.position.toInt()

                bars.forEach {
                    it?.max = maxProgress
                    it?.updateSeekBar(state, progressPos, maxProgress)
                    progressLabel?.updateSeekBar(state, progressPos, maxProgress)
                    remainLabel?.updateSeekBar(state, progressPos, maxProgress, true)
                }
            }
        }
    }

    registerCallback(callack)

}


private fun Any.updateSeekBar(
    state: PlaybackStateCompat,
    position: Int,
    maxProgress: Int,
    isRevesred: Boolean = false
) {
    progressAnimator.setIntValues(position, maxProgress)


    (if (isRevesred) maxProgress else position).let {
        if (this is ProgressBar) progress = it
        else if (this is TextView) text = convertToTime(it.toLong())
    }


    position.let {
        // If the media is playing then the seekbar should follow it, and the easiest
        // way to do that is to create a ValueAnimator to update it so the bar reaches
        // the end of the media the same time as playback gets there (or close enough).
        if (state.state == PlaybackStateCompat.STATE_PLAYING) {
            val timeToEnd = ((maxProgress - it) / state.playbackSpeed)

            if (timeToEnd >= 0) {
                progressAnimator.apply {
                    duration = timeToEnd.toLong()
                    interpolator = LinearInterpolator()
                    addUpdateListener { animator ->
                        (if (isRevesred) maxProgress - (animator.animatedValue as Int) else (animator.animatedValue as Int)).let {
                            if (this@updateSeekBar is ProgressBar) progress = it
                            if (this@updateSeekBar is TextView) text = convertToTime(it.toLong())
                        }
                    }
                }
                progressAnimator.start()
            }
        }
    }
}


fun convertToTime(millis: Long): String {
    val hours = millis / 1000 / 60 / 60
    val minutes = millis / 1000 / 60
    val seconds = millis / 1000 % 60

    return if (hours == 0L) String.format("%02d:%02d", minutes, seconds)
    else String.format("%02d:%02d:%02d", hours, minutes, seconds)
}

