/*
 * Copyright (C) 2019  Kavan Mevada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package elementx.media.services

import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.os.SystemClock
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaDescriptionCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.media.MediaBrowserServiceCompat
import elementx.media.extensions.setQueueList
import elementx.media.extensions.store
import elementx.media.mediacatalog.*
import elementx.media.models.MediaItem
import elementx.media.models.description
import elementx.media.models.metadataCompat
import elementx.media.notification.MediaNotification
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class MusicService : MediaBrowserServiceCompat() {

    override fun onGetRoot(clientPackageName: String, clientUid: Int, rootHints: Bundle?): BrowserRoot? {
        return BrowserRoot("root", rootHints)
    }

    override fun onLoadChildren(parentId: String, result: Result<List<MediaBrowserCompat.MediaItem>>) {
        MediaBrowser.getCatalog(baseContext, parentId, result)
    }

    private lateinit var mediaSession: MediaSessionCompat
    private lateinit var mediaNotification: MediaNotification
    private lateinit var audioManager: AudioManager

    private val playingQueue = mutableListOf<MediaSessionCompat.QueueItem>()
    private var queueIndex = -1
    private var currentMediaId = ""

    private var playOnAudioFocus = false

    val isPlaying get() = mediaPlayer != null && mediaPlayer!!.isPlaying

    private var mediaPlayer: MediaPlayer? = null
    private var playbackState: Int = PlaybackStateCompat.STATE_NONE

    private val availableActions: Long
        get() {
            var actions = (PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID
                    or PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH
                    or PlaybackStateCompat.ACTION_SKIP_TO_NEXT
                    or PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)
            actions = when (playbackState) {
                PlaybackStateCompat.STATE_STOPPED -> actions or (PlaybackStateCompat.ACTION_PLAY or PlaybackStateCompat.ACTION_PAUSE)
                PlaybackStateCompat.STATE_PLAYING -> actions or (PlaybackStateCompat.ACTION_STOP
                        or PlaybackStateCompat.ACTION_PAUSE
                        or PlaybackStateCompat.ACTION_SEEK_TO)
                PlaybackStateCompat.STATE_PAUSED -> actions or (PlaybackStateCompat.ACTION_PLAY or PlaybackStateCompat.ACTION_STOP)
                else -> actions or (PlaybackStateCompat.ACTION_PLAY
                        or PlaybackStateCompat.ACTION_PLAY_PAUSE
                        or PlaybackStateCompat.ACTION_STOP
                        or PlaybackStateCompat.ACTION_PAUSE)
            }
            return actions
        }


    private var mediaSessionCallback = object : MediaSessionCompat.Callback() {

        private val isReadyToPlay: Boolean
            get() = !playingQueue.isEmpty()

        override fun onAddQueueItem(description: MediaDescriptionCompat?) {
            playingQueue.add(MediaSessionCompat.QueueItem(description!!, description.hashCode().toLong()))
            mediaSession.setQueue(playingQueue)
        }

        override fun onRemoveQueueItem(description: MediaDescriptionCompat?) {
            playingQueue.remove(MediaSessionCompat.QueueItem(description!!, description.hashCode().toLong()))
            mediaSession.setQueue(playingQueue)
        }

        override fun onPrepare() {
            if (queueIndex < 0 && !isReadyToPlay) {
                Log.d(TAG, "Nothing to play.")
                return
            }

            mediaSession.setMetadata(playingQueue[queueIndex].metadataCompat(baseContext))

            if (!mediaSession.isActive) mediaSession.isActive = true

            setNewState(playbackState)
        }

        override fun onPlayFromMediaId(mediaId: String?, extras: Bundle?) {
            val id = mediaId?.split("_")
            GlobalScope.launch {
                when (id?.get(0)) {
                    "ALBM" -> retrieveMediaFromAlbum(id[1])?.let {
                        mediaSession.controller.setQueueList(baseContext, it)
                    }
                    "ARTS" -> retrieveMediaFromArtist(id[1])?.let {
                        mediaSession.controller.setQueueList(baseContext, it)
                    }
                    "PLST" -> retrieveMediaFromPlaylist(id[1])?.let {
                        mediaSession.controller.setQueueList(baseContext, it)
                    }
                    "GNRE" -> retrieveMediaFromGenre(id[1])?.let {
                        mediaSession.controller.setQueueList(baseContext, it)
                    }
                    "SHUFFLE_ALL" -> retrieveMedia()?.let {
                        it.shuffle()
                        mediaSession.controller.setQueueList(baseContext, it)
                    }
                }

                onSkipToQueueItem(0L)
                if (!isPlaying) onPlay()
            }
        }

        override fun onPlay() {
            if (isReadyToPlay) {
                if (playingQueue[queueIndex].description.mediaId != currentMediaId) onPrepare()

                if (playbackState == PlaybackStateCompat.STATE_PAUSED
                    && playingQueue[queueIndex].description.mediaId == currentMediaId
                ) {
                    play()
                    return
                }

                releasePlayer()

                // Initialize mediaPlayer
                if (mediaPlayer == null) {
                    mediaPlayer = MediaPlayer()
                    mediaPlayer!!.setOnCompletionListener {
                        //mPlaybackInfoListener.onPlaybackCompleted()
                        // The media player finished playing the current song, so we go ahead
                        // and start the next.
                        when (mediaSession.controller.repeatMode) {
                            PlaybackStateCompat.REPEAT_MODE_ONE -> onPlay()
                            PlaybackStateCompat.REPEAT_MODE_ALL -> {

                            }
                            PlaybackStateCompat.REPEAT_MODE_NONE -> {
                                if (queueIndex < playingQueue.size - 1) {
                                    onSkipToNext()
                                    onPlay()
                                } else {
                                    setNewState(PlaybackStateCompat.STATE_PAUSED)
                                    onStop()
                                }
                            }
                        }
                    }
                }

                currentMediaId = playingQueue[queueIndex].description.mediaId!!
                val mediaUri = playingQueue[queueIndex].description.mediaUri
                try {
                    mediaPlayer!!.setDataSource(baseContext, mediaUri!!)
                    mediaPlayer!!.prepare()
                } catch (e: Exception) {
                    throw RuntimeException("Failed to set file Uri: $mediaUri", e)
                }

                play()

                Log.d(TAG, "onPlayFromMediaId: MediaSession active")

            } else Log.d(TAG, "Playing Queue is empty!")

        }

        override fun onPause() = pause()

        override fun onStop() = stop()

        override fun onSkipToNext() {
            if (isReadyToPlay) {
                onSkipToQueueItem((++queueIndex % playingQueue.size).toLong())
            } else Log.d(TAG, "Playing Queue is empty!")
        }

        override fun onSkipToPrevious() {
            if (isReadyToPlay) {
                val index = --queueIndex
                onSkipToQueueItem((if (index < 0) playingQueue.size - 1 else index).toLong())
            } else Log.d(TAG, "Playing Queue is empty!")
        }

        override fun onSkipToQueueItem(id: Long) {
            queueIndex =
                if (id == -1L) 0
                else if (!isReadyToPlay) -1
                else (id).toInt()

            // send queue position in media session via extras
            mediaSession.setExtras(Bundle().apply {
                putInt("position", queueIndex)
                putString("current_mediaid", currentMediaId)
            })

            store("queueIndex", queueIndex) // Save Position

            if (isPlaying) onPlay() else onPrepare()
        }

        override fun onSeekTo(pos: Long) {
            if (mediaPlayer != null) {
                mediaPlayer!!.seekTo(pos.toInt())

                // Set the state (to the current state) because the position changed and should
                // be reported to clients.
                setNewState(playbackState)
            }
        }

        override fun onCustomAction(action: String?, extras: Bundle?) {
            when (action) {
                "setQueue" -> {
                    val array =
                        extras?.getParcelableArray("queue")?.map {
                            val description = (it as MediaItem).description
                            MediaSessionCompat.QueueItem(description!!, description.hashCode().toLong())
                        }
                    array?.let {
                        playingQueue.clear()
                        playingQueue.addAll(array)
                        mediaSession.setQueue(array)
                    }
                }
                "thumbs_up" -> {
                    //val mediaId = playingQueue[queueIndex].mediaId
                    //setFavorite(mediaId, !isFavorite(mediaId))
                    //setNewState(playbackState)
                }
            }
        }


        override fun onSetRepeatMode(repeatMode: Int) {
            mediaSession.setRepeatMode(repeatMode)
            println("onRepeatMode $repeatMode")
        }

        override fun onSetShuffleMode(shuffleMode: Int) {
            if (shuffleMode == PlaybackStateCompat.SHUFFLE_MODE_ALL) {
                playingQueue.shuffle()
            }
            mediaSession.setQueue(playingQueue)
            onSkipToQueueItem(0)
        }
    }

    fun play() {
        val result = audioManager.requestAudioFocus(
            audioFocusChangeListener,
            AudioManager.STREAM_MUSIC,
            AudioManager.AUDIOFOCUS_GAIN
        )

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            //registerAudioNoisyReceiver here
            if (mediaPlayer != null && !mediaPlayer!!.isPlaying) {
                mediaPlayer!!.start()
                setNewState(PlaybackStateCompat.STATE_PLAYING)
            }
        }
    }


    fun pause() {
        if (!playOnAudioFocus) {
            audioManager.abandonAudioFocus(audioFocusChangeListener)
        }

        //unregisterAudioNoisyReceiver here
        if (mediaPlayer != null && mediaPlayer!!.isPlaying) {
            mediaPlayer!!.pause()
            setNewState(PlaybackStateCompat.STATE_PAUSED)
        }
    }

    fun stop() {
        audioManager.abandonAudioFocus(audioFocusChangeListener)
        //unregisterAudioNoisyReceiver here

        // Regardless of whether or not the MediaPlayer has been created / started, the state must
        // be updated, so that MediaNotification can take down the notification.
        setNewState(PlaybackStateCompat.STATE_STOPPED)
        releasePlayer()
        mediaSession.isActive = false
    }


    override fun onCreate() {
        super.onCreate()

        // Create a new MediaSession.
        mediaSession = MediaSessionCompat(this, "MusicService")
        mediaSession.setCallback(mediaSessionCallback)
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_QUEUE_COMMANDS)
        sessionToken = mediaSession.sessionToken

        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager


        mediaNotification = MediaNotification(this)
        Log.d(TAG, "onCreate: MusicService creating MediaSession, and MediaNotification")
    }

    override fun onTaskRemoved(rootIntent: Intent) {
        super.onTaskRemoved(rootIntent)
        stopSelf()
    }


    override fun onDestroy() {
        stop()
        mediaSession.release()
        Log.d(TAG, "onDestroy: MediaPlayerAdapter stopped, and MediaSession released")
    }

    private fun releasePlayer() {
        if (mediaPlayer != null) {
            mediaPlayer!!.release()
            mediaPlayer = null
        }
    }

    private fun moveServiceToStartedState(state: PlaybackStateCompat) {
        val notification =
            mediaNotification.getNotification(
                playingQueue[queueIndex].metadataCompat(baseContext),
                state,
                sessionToken!!
            )

        if (!serviceInStartedState) {
            ContextCompat.startForegroundService(
                this@MusicService,
                Intent(this@MusicService, MusicService::class.java)
            )
            serviceInStartedState = true
        }

        startForeground(MediaNotification.NOTIFICATION_ID, notification)
    }

    private fun updateNotificationForPause(state: PlaybackStateCompat) {
        if (!isPlaying) stopForeground(false)
        val notification = mediaNotification.getNotification(
            playingQueue[queueIndex].metadataCompat(baseContext), state, sessionToken!!
        )
        mediaNotification.notificationManager
            .notify(MediaNotification.NOTIFICATION_ID, notification)
    }

    private fun moveServiceOutOfStartedState() {
        stopForeground(true)
        stopSelf()
        serviceInStartedState = false
    }


    // This is the main reducer for the player state machine.
    private fun setNewState(@PlaybackStateCompat.State newPlayerState: Int) {
        playbackState = newPlayerState

        // Work around for MediaPlayer.getCurrentPosition() when it changes while not playing.
        val reportPosition = (if (mediaPlayer == null) 0 else mediaPlayer!!.currentPosition).toLong()
        val reportDuration = (if (mediaPlayer == null) 0 else mediaPlayer!!.duration)

        val stateBuilder = PlaybackStateCompat.Builder()
        stateBuilder.setActions(availableActions)
        //Set duration to mediaSession for seekbar/progressbar
        stateBuilder.setExtras(Bundle().apply { putInt("duration", reportDuration) })
        stateBuilder.setState(
            playbackState,
            reportPosition,
            1.0f,
            SystemClock.elapsedRealtime()
        )

        val state = stateBuilder.build()

        // Report the state to the MediaSession.
        mediaSession.setPlaybackState(state)

        // Manage the started state of this service.
        when (state.state) {
            PlaybackStateCompat.STATE_PLAYING -> moveServiceToStartedState(state)
            PlaybackStateCompat.STATE_PAUSED -> updateNotificationForPause(state)
            PlaybackStateCompat.STATE_STOPPED -> moveServiceOutOfStartedState()
        }
    }

    /**
     * Helper class for managing audio focus related tasks.
     */
    private val audioFocusChangeListener = object : AudioManager.OnAudioFocusChangeListener {
        override fun onAudioFocusChange(focusChange: Int) {
            when (focusChange) {
                AudioManager.AUDIOFOCUS_GAIN -> {
                    if (playOnAudioFocus && !isPlaying) {
                        play()
                    } else if (isPlaying) {
                        if (mediaPlayer != null) {
                            mediaPlayer!!.setVolume(
                                MEDIA_VOLUME_DEFAULT,
                                MEDIA_VOLUME_DEFAULT
                            )
                        }
                    }
                    playOnAudioFocus = false
                }
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> if (mediaPlayer != null) {
                    mediaPlayer!!.setVolume(
                        MEDIA_VOLUME_DUCK,
                        MEDIA_VOLUME_DUCK
                    )
                }
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> if (isPlaying) {
                    playOnAudioFocus = true
                    pause()
                }
                AudioManager.AUDIOFOCUS_LOSS -> {
                    audioManager.abandonAudioFocus(this)
                    playOnAudioFocus = false
                    pause()
                }
            }
        }
    }

    companion object {
        var serviceInStartedState: Boolean = false

        private val TAG = MusicService::class.java.simpleName
        private const val MEDIA_VOLUME_DEFAULT = 1.0f
        private const val MEDIA_VOLUME_DUCK = 0.2f
    }

}

