package elementx.media.interfaces

import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat

interface MediaSessionCallback {
    fun onMetadataChanged(metadata: MediaMetadataCompat, queuIndex: Int)
    fun onPlaybackStateChanged(state: PlaybackStateCompat)
    fun onServiceConnected(controller: MediaControllerCompat)
    fun onQueueChanged(queue: MutableList<MediaSessionCompat.QueueItem>?)
    fun onRepeatModeChanged(repeatMode: Int)
    fun onShuffleModeChanged(shuffleMode: Int)
}